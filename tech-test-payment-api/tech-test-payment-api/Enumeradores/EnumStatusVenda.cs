﻿namespace tech_test_payment_api.Enumeradores
{
    public enum EnumStatusVenda
    {
        Cancelada = 1,
        Entregue = 2,
        EnviadoParaTransportadora = 3,
        PagamentoAprovado = 4

    }
}
